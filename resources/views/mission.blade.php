@extends('shell')

@section('content')
    <div class="card card-main">
        <div class="card-content">
            <div class="card-header">
                Mission
            </div>
            <div class="card-body mb-3">
                <p>Our mission started with a simple thought:</p>
                <div class="quote-container ">
                    <div class="quote">"There must be a way to force birds to do work for me for free."</div>
                    <div class="source">-Greg Browning</div>
                </div>
            </div>
        </div>
    </div>

    <div class="huge-row">
        <div class="huge-row-content text-logo background-logo-icon">
            <h2 class="mb-4 text-center"><strong>Moving data isn't easy</strong></h2>
            <p class="text-white">One day, while trapped inside his upturned Jeep, Greg spotted some birds overhead.</p>
            <p class="text-white">Greg quickly put together a list of every bird fact he knew:<p>
            <ul class="text-logo pl-4" style="list-style-type: none; font-size: 1.25em;">
                <li>*Birds are dumb, but also kinda smart</li>
                <li>*Capable of flight</li>
                <li>*Large talons</li>
                <li>*Immune to clouds?</li>
                <li>*Oh god my head hurts</li>
            </ul>
        </div>
        <div class="huge-row-image" style="background-image: url('images/pigeons-openup.png')">
            
        </div>
    </div>

    <div class="huge-row">
        <div class="huge-row-image text-secondary text-right pt-2 pr-3" style="background-image: url('images/baldboys.png')">
            #baldboys    
        </div>
        <div class="huge-row-content text-logo-icon background-logo">
            <h2 class="mb-4 text-center"><strong>Birds obey when trained and bred properly</strong></h2>
            <p class="text-white">Greg realized he could use these strange creatures to get things done. And, unlike people, birds ALWAYS do what Greg says.</p>
            <p>#birdslisten</p>
            <p class="text-white">Single Pigeon Global Pigeons are bred from top quality pigeons to exert dominance and fly with conviction.</p>
        </div>
    </div>

    <div class="huge-row">
        <div class="huge-row-content text-logo background-logo-icon">
            <h2 class="mb-4 text-center"><strong>Are there other pigeons? Why is it Single Pigeon Global? Is that one pigeon global-capable? Can pigeons fly across the ocean?</strong></h2>
            <p class="text-white">You sure ask a lot of dumb questions. You'll be hearing from our bird lawyers.</p>
        </div>
        <div class="huge-row-image" style="background-image: url('images/birdlaw.png')">
            
        </div>
    </div>

    <div class="mt-4 mb-4'">...</div>

    <style>
        p {
            font-size: 1.5em;
        }

        .huge-row {
            display: flex;
            margin: 0px;
            padding: 0px;
            min-height: 400px;
        }

        .huge-row-content {
            padding: 2em;
            width: 33%
        }

        .huge-row-image {
            padding: 0px;
            width: 67%;
        }

        .card-main {
            border: none;
            border-radius: 0px;
            margin-top: 3em;
            margin-bottom: 3em;
            background-color: #559F41;
            color: white;            
        }

        .card-header {
            background-color: inherit;
            font-weight: bold;
            font-size: 2.5em;
            width: 50%;
            margin: auto;
            text-align: center;
            border: none;        
        }

        .card-content {
            max-width: 1000px;
            margin: auto;
        }

        .quote-container {
            color: #F1CC07;
            width: 100%;
            margin: auto;
        }

        .quote-container .quote {
            font-size: 3em;
            font-style: italic;
        }

        .quote-container .source {
            position: relative;
            width: 100%;
            text-align: right;
        }
    </style>

@endsection