@extends('shell')

@section('content')
        <div class="w-100 background-logo m-0 p-0">
            <div class="flex-center position-ref">
                <div class="content">
                    <div class="jumbotron background-logo text-logo-icon w-100" style="border-radius: 0px;">
                        <div class='row'>
                            <div class='col-6 text-right'>
                                <img src='/images/profile.png' class='logo-icon-jumbo'>
                            </div>
                            <div class='col-6 text-left'>
                                <div style="max-width: 600px;">
                                <h1>Single Pigeon Global</h1>
                                <p class="lead text-white">When your data simply, absolutely, must, in any possible way, shape or form, no ifs, ands, or buts about it, regardless of any ridiculous social norms or legal consequences, <strong>has</strong> to be there.</p>
                                <hr class="my-4">
                                <h3>Simple. Better. Avian.</h3>
                                <div class="text-center">
                                    <a class="btn btn-logo btn-xl mt-4" href="#" role="button" onclick="HowAlert()">Ask Us: How?</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        function HowAlert() {
            alert('Please save me. Greg has forced me into this. I need help. GET ME OUT OF HERE CALL CPS!!!!!')
        }
    </script>
@endsection