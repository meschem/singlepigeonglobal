<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <!-- Scripts -->
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha256-pasqAKBDmFT4eHoN2ndd6lN370kFiGUFyTiUHWhU7k8=" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: calc(100vh - 60px);
                margin: 0;
            }

            .full-height {
                height: calc(100vh - 60px);
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .content {
                text-align: center;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .text-logo-icon {
                color: #F1CC07;
            }

            .background-logo {
                background-color: #559F41;
            }

            .logo-icon {
                width: 4em;
                height: 4em;
            }

            .btn-logo {
                color: #F1CC07;
                background-color: #559F41;
                box-shadow: 0px 0px 0px #F1CC07;
            }

            .btn-logo:hover, .btn-logo:active {
                transform: translateX(-4px) translateY(-4px);
                transition-delay: 0ms;
                color: #F1CC07;
                border-color: #F1CC07;
                background-color: #559F41;
                box-shadow: 4px 4px 0px #F1CC07;
            }

            .navbar-logo {
                background-color: #fff;
                color: #559F41;
            }

            .navbar-logo a {
                color: #F1CC07;
            }

            .navbar-brand {
                background-color: #559F41;
                color: #F1CC07;
                padding-right: 2em;
                padding-left: 1em;
                border-radius: 4px;
            }

            .logo-icon-nav {
                width: 4em;
                height: 4em;
                position: relative;
                top: -8px;
            }

            .logo-icon-jumbo {
                height: 400px;
                width: 400px;
            }

            .btn-xl {
                width: 400px;
                height: 3em;
                font-size: 2em;
                font-weight: bold;
                padding-top: 0.75em;
            }

            .custom-navbar {
                margin: 0px;
                padding: 0.3em;
                height: 5em;
                padding-top: 1.25em;
                color: #559F41;
            }

            .custom-navbar a {
                margin-right: 2em;
                color: #559F41;
                font-size: 1.25em;
                font-weight: bold;
                top: -0.25em;
                position: relative;
            }

            .nav-divider {
                margin-left: 1em;
                margin-right: 1em;
                font-size: 2.75em;
                top: -0.25em;
                position: relative;
            }

            /* {"bg":"#559F41","icon":"#F1CC07","font":"#FFFFFF","slogan":"#ffffff"} */      
        </style>
    </head>
    <body>
        
        <div class="custom-navbar flex-center position-ref">
            <div class="d-inline">
                <img src="/images/profile-green.png" class="logo-icon-nav">
                <h3 class="d-inline">Single Pigeon Global</h3>                
            </div>
            <span class="nav-divider"> | </span>
            <a href="mission">Mission</a>
            <a href="mission">People</a>
            <a href="mission">Give Us Money</a>
        </div>

        <div class="w-100 background-logo m-0 p-0">
            <div class="flex-center position-ref">
                <div class="content">
                    <div class="jumbotron background-logo text-logo-icon w-100" style="border-radius: 0px;">
                        <div class='row'>
                            <div class='col-6 text-right'>
                                <img src='/images/profile.png' class='logo-icon-jumbo'>
                            </div>
                            <div class='col-6 text-left'>
                                <div style="max-width: 600px;">
                                <h1>Single Pigeon Global</h1>
                                <p class="lead text-white">When your data simply, absolutely, must, in any possible way, shape or form, no ifs, ands, or buts about it, regardless of any ridiculous social norms or legal consequences, <strong>has</strong> to be there.</p>
                                <hr class="my-4">
                                <h3>Simple. Better. Avian.</h3>
                                <div class="text-center">
                                    <a class="btn btn-logo btn-xl mt-4" href="#" role="button" onclick="HowAlert()">Ask Us: How?</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        function HowAlert() {
            alert('Please save me. Greg has forced me into this. I need help. GET ME OUT OF HERE CALL CPS!!!!!')
        }
    </script>
    </body>
</html>
