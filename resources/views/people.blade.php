@extends('shell')

@section('content')
    <div class="card card-main">
        <div class="card-content">
            <div class="card-header">
                People
            </div>
            <div class="card-body mb-3">
                <p>Greg loves birds, but as Greg always says:</p>
                <div class="quote-container ">
                    <div class="quote">"People are a close second. I'm a people."</div>
                    <div class="source">-Greg Browning</div>
                </div>
            </div>
        </div>
    </div>

    <div class="big-row">
        <div class="big-row-image" style="background-image: url('images/greg.png')"></div>        
        <div class="big-row-content">
            <div class="text-logo"><h2 class="mb-4 text-logo d-inline"><strong>Gregory Browning</strong></h2> CEO and Bird-President</div>
            <p class="text-dark mt-3">The world may never know Greg's true origins, but suffice to say he is a man of 'tegridy.</p>
            <p class="text-dark">
                Greg inspects all birds on a daily basis, "insuring" only the best pigeons make the cut. 
                Pigeon don't listen? Fired.
                Pigeon don't squawk? Fired.
                Pigeon don't fly? Fired.
                Pigeon taste delicious? Fired. Grilled. Eaten.
            </p>
        </div>
    </div>

    <div class="big-row">
        <div class="big-row-image" style="background-image: url('images/michael.png')"></div>        
        <div class="big-row-content">
            <div class="text-logo"><h2 class="mb-4 text-logo d-inline"><strong>Michael Boback</strong></h2> Birds whisperer</div>
            <p class="text-dark mt-3">Michael has been with Greg since nearly the beginning for some reason.</p>
            <p class="text-dark">When not working Michael enjoys spending his time working.</p>
        </div>
    </div>

    <div class="big-row">
        <div class="big-row-image" style="background-image: url('images/bobby.png')"></div>        
        <div class="big-row-content">
            <div class="text-logo"><h2 class="mb-4 text-logo d-inline"><strong>Bobby "Smokin' Guns" Lanier</strong></h2> Definitely Something</div>
            <p class="text-dark mt-3">No idea what this guy does, but he seems to think he's important.</p>
        </div>
    </div>

    <div class="big-row">
        <div class="big-row-image" style="background-image: url('images/matt.png')"></div>        
        <div class="big-row-content">
            <div class="text-logo"><h2 class="mb-4 text-logo d-inline"><strong>Matt Schemmer</strong></h2> Web Developer</div>
            <p class="text-dark mt-3">The tale of Matt Schemmer is a long and beguiling one, best shared over a campfire with a bottle of whiskey.</p>
            <p class="text-dark">
                Some say he spends too much time making fun of other people, when the real demons he wishes to quash are within himself. 
                Will he ever realize this?
                Or will he continue his days bringing down others?
            </p>
        </div>
    </div>

    <div class="mt-4 mb-4'">...</div>

    <style>
        p {
            font-size: 1.5em;
        }

        .big-row {
            display: flex;
            margin: auto;
            padding: 0px;
            height: 300px;
            max-width: 1200px;
            margin-bottom: 3em;
        }

        .big-row-content {
            padding-left: 1em;
            padding-top: 0.25em;
            width: 900px
        }

        .big-row-image {
            padding: 0px;
            width: 300px;
        }

        .card-main {
            border: none;
            border-radius: 0px;
            margin-top: 3em;
            margin-bottom: 3em;
            background-color: #559F41;
            color: white;            
        }

        .card-header {
            background-color: inherit;
            font-weight: bold;
            font-size: 2.5em;
            width: 50%;
            margin: auto;
            text-align: center;
            border: none;        
        }

        .card-content {
            max-width: 1000px;
            margin: auto;
        }

        .quote-container {
            color: #F1CC07;
            width: 100%;
            margin: auto;
        }

        .quote-container .quote {
            font-size: 3em;
            font-style: italic;
        }

        .quote-container .source {
            position: relative;
            width: 100%;
            text-align: right;
        }
    </style>

@endsection